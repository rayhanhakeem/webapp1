﻿namespace WebApplication1.Models
{
	public class MyConfiguration
	{
		public string Key { get; set; }
		public string Issuer { get; set; }
		public string Audience { get; set; }
	}
}
