﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System;
using WebApplication1.Context;
using WebApplication1.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebApplication1.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{

		private readonly IOptions<MyConfiguration> ConfigurationOption;
		private readonly ApplicationDbContext Context;

		public UserController(ApplicationDbContext _context, IOptions<MyConfiguration> _configurationOption)
		{
			Context = _context;
			ConfigurationOption = _configurationOption;
		}

		//Tambahin Data ke Database pake Hardcode
		/*[HttpPost("Coba")]
		public async Task CobaDulu()
		{

			this.Context.Users.Add(new User
			{
				Username = "rayhan_admin",
				EmailAddress = "rayhan.admin@gmail.com",
				Password = "MyPass_w0rd",
				GivenName = "Rayhan",
				Surname = "Tampan",
				Role = "Administrator"
			});

			await this.Context.SaveChangesAsync();
		}*/

		//Ambil Inputan dari user buat ditaro di database (CREATE)
		[HttpPost("register")]
		public async Task<ActionResult<User>> TerimaInput([FromBody] User user)
		{
			var caridulu = await this.Context.Users
				.AsNoTracking()
				.Where(Q => Q.Username == user.Username)
				.FirstOrDefaultAsync();

			if (caridulu != null)
			{
				return BadRequest("Udah ada mas usernamenya..");
			}
			var salt = "S5U1tM3di4";

			var HashedPW = HashPassword($"{user.Password}{salt}");

			this.Context.Users.Add(new User
			{
				Id = user.Id,
				Username = user.Username,
				EmailAddress = user.EmailAddress,
				Password = HashedPW,
				GivenName = user.GivenName,
				Surname = user.Surname,
				Role = user.Role
			});

			if (user.Role != "Administrator" && user.Role != "Standard")
			{
				var test = "Role harus diisi dangan 'Administrator' atau 'Standard'";
				return BadRequest(test);
			}

			await this.Context.SaveChangesAsync();
			return Ok(user);
		}

		[HttpPost("login")]
		public async Task<ActionResult<User>> Login([FromBody] UserLogin user)
		{
			var salt = "S5U1tM3di4";
			if (!string.IsNullOrEmpty(user.Username) &&
				!string.IsNullOrEmpty(user.Password))
			{
				var loggedInUser = await this.Context.Users
					.Where(Q => Q.Username == user.Username && Q.Password == HashPassword($"{user.Password}{salt}"))
					.FirstOrDefaultAsync();
				if (loggedInUser is null) return NotFound("Username atau Password salah");

				var claims = new[]
				{
					new Claim(ClaimTypes.NameIdentifier, loggedInUser.Username),
					new Claim(ClaimTypes.Email, loggedInUser.EmailAddress),
					new Claim(ClaimTypes.GivenName, loggedInUser.GivenName),
					new Claim(ClaimTypes.Surname, loggedInUser.Surname),
					new Claim(ClaimTypes.Role, loggedInUser.Role),
					new Claim(ClaimTypes.Name, loggedInUser.Id.ToString())
				};

				var token = new JwtSecurityToken
				(
					issuer: this.ConfigurationOption.Value.Issuer,
					audience: this.ConfigurationOption.Value.Audience,
				claims: claims,
				expires: DateTime.UtcNow.AddDays(60),
				notBefore: DateTime.UtcNow,
					signingCredentials: new SigningCredentials(
						new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.ConfigurationOption.Value.Key)),
						SecurityAlgorithms.HmacSha256)
				);

				var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
				return Ok(tokenString);
			}
			return BadRequest("Invalid user credentials");
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
		//read list semua user (READ)
		[HttpGet("list")]
		public async Task<List<User>> CobaAmbil()
		{

			var list = await this.Context.Users
				.AsNoTracking()
				.OrderBy(Q => Q.Id)
				.ToListAsync();

			return list;
		}

		//
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
		[HttpPost("get")]
		public async Task<ActionResult<User>> CaraQuery([FromQuery] int id)
		{
			var userDb = await this.Context.Users
				.AsNoTracking()
				.Where(Q => Q.Id == id)
				.FirstOrDefaultAsync();

			if (userDb == null)
			{
				return BadRequest("Ga ada usernya");
			}

			return Ok(userDb);
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
		[HttpPut("update")]
		public async Task<ActionResult> Update([FromBody] User newUser)
		{

			var salt = "S5U1tM3di4";
			var existingUser = await this.Context.Users
				.AsNoTracking()
				.Where(Q => Q.Id == newUser.Id)
				.FirstOrDefaultAsync();

			/*var caridulu = await this.Context.Users
				.AsNoTracking()
				.Where(Q => Q.Username == user.Username)
				.FirstOrDefaultAsync();
*/
			if (existingUser == null)
			{
				return BadRequest("Ga ada usernya");
			}

			existingUser.Username = newUser.Username;
			existingUser.Password = newUser.Password;
			existingUser.GivenName = newUser.GivenName;
			existingUser.Surname = newUser.Surname;
			existingUser.Role = newUser.Role;

			if (existingUser.Username == newUser.Username)
			{
				return BadRequest("Udah ada mas usernamenya..");
			}

			var newHashedPW = HashPassword($"{newUser.Password}{salt}");

			if (existingUser.Password != newHashedPW)
			{
				existingUser.Password = newHashedPW;
			}

			if (newUser.Role != "Administrator" && newUser.Role != "Standard")
			{
				var test = "Role harus diisi dangan 'Administrator' atau 'Standard'";
				return BadRequest(test);
			}

			this.Context.Users.Update(existingUser);
			await this.Context.SaveChangesAsync();

			return Ok("Updated");
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
		[HttpDelete("delete")]
		public async Task<ActionResult<User>> Delete([FromQuery] int id)
		{
			var oldAccount = this.Context.Users.Where(Q => Q.Id == id).FirstOrDefault();

			if (oldAccount == null)
			{
				return NotFound("Akunnya gaada");
			}

			this.Context.Users.Remove(oldAccount);
			await this.Context.SaveChangesAsync();

			return Ok(oldAccount);
		}
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
		[HttpGet("getCurrentUser")]
		public async Task<IActionResult> getCurrentUser()
		{
			/*int id = Convert.ToInt32(HttpContext.User.FindFirstValue("id"));
			return Ok(new {  = id });*/
			string userId = User.FindFirst(ClaimTypes.Name)?.Value;
			var userData = Context.Users.FirstOrDefault(x => x.Id == Convert.ToInt32(userId));

			return Ok(new { CurrentUser = userData });

		}

		private string HashPassword(string password)
		{
			SHA256 hash = SHA256.Create();

			var passwordBytes = Encoding.Default.GetBytes(password);

			var hashedpassword = hash.ComputeHash(passwordBytes);

			return Convert.ToBase64String(hashedpassword);
		}
	}
}
