﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Context;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
	public class MovieController : ControllerBase
	{
		private readonly ApplicationDbContext Context;

		public MovieController(ApplicationDbContext _context)
		{
			Context = _context;
		}

		[HttpPost("create")]
		public async Task<ActionResult> TerimaInput([FromBody] Movie movie)
		{
			var caridulu = await this.Context.Movies
				.AsNoTracking()
				.Where(Q => Q.Title == movie.Title)
				.FirstOrDefaultAsync();

			if (caridulu != null)
			{
				return BadRequest("Udah ada mas filmnya..");
			}
			this.Context.Movies.Add(new Movie
			{
				Id = movie.Id,
				Title = movie.Title,
				Description = movie.Description,
				Rating = movie.Rating,
			});

			await this.Context.SaveChangesAsync();
			return Ok(movie);
		}

		//read list semua user (READ)

		[HttpGet("list")]
		public async Task<List<Movie>> CobaAmbil()
		{

			var list = await this.Context.Movies
				.AsNoTracking()
				.OrderBy(Q => Q.Id)
				.ToListAsync();

			return list;
		}

		//
		[HttpPost("get")]
		public async Task<ActionResult<Movie>> CaraQuery([FromQuery] int id)
		{
			var userDb = await this.Context.Movies
				.AsNoTracking()
				.Where(Q => Q.Id == id)
				.FirstOrDefaultAsync();

			if (userDb == null)
			{
				return BadRequest("Ga ada gan");
			}

			return Ok(userDb);
		}

		[HttpPut("update")]
		public async Task<ActionResult> Update([FromBody] Movie newMovie)
		{
			var existingMovie = await this.Context.Movies
				.AsNoTracking()
				.Where(Q => Q.Id == newMovie.Id)
				.FirstOrDefaultAsync();

			if (existingMovie == null)
			{
				return BadRequest("Ga ada filmnya mas");
			}

			this.Context.Movies.Update(new Movie
			{
				Id = newMovie.Id,
				Title = newMovie.Title,
				Description = newMovie.Description,
				Rating = newMovie.Rating,
			});

			/*existingMovie.Id = newMovie.Id;
			existingMovie.Title = newMovie.Title;
			existingMovie.Description = newMovie.Description;
			existingMovie.Rating = newMovie.Rating;

			this.Context.Movies.Update(existingMovie);*/
			await this.Context.SaveChangesAsync();

			return Ok("Updated");
		}

		[HttpDelete("delete")]
		public async Task<ActionResult<Movie>> Delete([FromQuery] int id)
		{
			var oldMovie = this.Context.Movies.Where(Q => Q.Id == id).FirstOrDefault();

			if (oldMovie == null)
			{
				return NotFound();
			}

			this.Context.Movies.Remove(oldMovie);
			await this.Context.SaveChangesAsync();

			return Ok(oldMovie);
		}


	}
}
