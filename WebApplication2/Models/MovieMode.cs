﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
	public class MovieMode
	{
		[Key]
		public int Id { get; set; }
		public string Title { get; set; }
		public string? Description { get; set; }
		public string? Rating { get; set; }
	}
}
