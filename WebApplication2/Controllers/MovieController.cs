﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WebApplication2.Areas.Identity.Data;
using WebApplication2.Data;
using WebApplication2.Models;
namespace WebApplication2.Controllers
{
	[ApiController]
	[Route("[controller]")]
	[Authorize]
	public class MovieController : ControllerBase
	{
		private WebApplication2Context _context;

		public MovieController(WebApplication2Context context)
		{
			_context = context;
		}

		[HttpPost("create")]
		public async Task<ActionResult> Create([FromBody]MovieMode movie)
		{
			_context.Movies.Add(movie);

			await _context.SaveChangesAsync();
			return Ok(new { Movie = movie });
		}

		[HttpGet("list")]
		public async Task<List<MovieMode>> listMovie()
		{
			var list = await _context.Movies
				.AsNoTracking()
				.OrderBy(x => x.Id)
				.ToListAsync();

			return list;
		}

		[HttpGet("get")]
		public async Task<ActionResult> getMovie([FromQuery] int id)
		{
			var userExist = await _context.Movies
				.AsNoTracking()
				.Where(Q => Q.Id == id)
				.FirstOrDefaultAsync();
			if(userExist == null) 
			{ 
				return BadRequest("Gaada cok!");
			}
			return Ok(userExist);
		}

		[HttpPut("update")]
		public async Task<ActionResult> Update([FromBody]MovieMode newMovie)
		{
			var cariDulu = await _context.Movies
				.AsNoTracking()
				.Where(Q => Q.Id == newMovie.Id)
				.FirstOrDefaultAsync();
			if(cariDulu == null)
			{
				return BadRequest("Akunnya gaada!");
			}

			_context.Movies.Update(newMovie);

			await _context.SaveChangesAsync();
			return Ok(newMovie);
		}

		[HttpDelete("delete")] 
		public async Task<ActionResult> Delete([FromQuery]int id)
		{
			var userExist = await _context.Movies.Where(Q=>Q.Id==id).FirstOrDefaultAsync();
			if(userExist == null)
			{
				return BadRequest("Gaada akunnya!");
			}
			_context.Movies.Remove(userExist);
			await _context.SaveChangesAsync();
			return Ok(new { Deleted = userExist });
		}
	}

}
