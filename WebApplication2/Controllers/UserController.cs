﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NuGet.Protocol.Plugins;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WebApplication2.Areas.Identity.Data;
using WebApplication2.Data;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
	[ApiController]
	[Route("[controller]")]
	[Authorize]
	public class UserController : ControllerBase
	{
		private WebApplication2Context _context;
		private readonly UserManager<WebApplication2User> _userManager;
		private readonly SignInManager<WebApplication2User> _signInManager;

		public UserController(WebApplication2Context context, UserManager<WebApplication2User> userManager, SignInManager<WebApplication2User> signInManager)
		{
			_context = context;
			_userManager = userManager;
			_signInManager = signInManager;
		}

		[AllowAnonymous]
		[HttpPost("register")]
		public async Task<ActionResult> Register([FromBody] UserModel userModel)
		{
			WebApplication2User webApplication2User = new WebApplication2User()
			{
				UserName = userModel.Username,
				Email = userModel.Email,
				EmailConfirmed = true
			};

			var result = await _userManager.CreateAsync(webApplication2User, userModel.Password);

			if (result.Succeeded)
			{
				return Ok(new { Result = "Berhasil Cuy" });
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (var error in result.Errors)
				{
					stringBuilder.Append(error.Description);
					stringBuilder.Append("\r\n");
				}
				return Ok(new { Result = $"Register Fail: {stringBuilder.ToString()}" });
			}
		}

		[AllowAnonymous]
		[HttpPost("login")]
		public async Task<ActionResult> Login([FromBody] MyLoginModel myLoginModel)
		{
			var user = _context.Users.FirstOrDefault(x => x.UserName == myLoginModel.Username);

			if (user != null)
			{
				var signInResult = await _signInManager.CheckPasswordSignInAsync(user, myLoginModel.Password, false);

				if (signInResult.Succeeded)
				{
					var tokenHandler = new JwtSecurityTokenHandler();
					var key = Encoding.ASCII.GetBytes("ASZ_2421_LGS_KEY_KMDNAJQLWROPXZY%@$%{})*+_}|><");
					var tokenDescriptor = new SecurityTokenDescriptor
					{
						Subject = new ClaimsIdentity(new Claim[]
						{
							new Claim(ClaimTypes.Name, myLoginModel.Username),
							new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
						}),
						Expires = DateTime.UtcNow.AddMinutes(30),
						SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
					};
					var token = tokenHandler.CreateToken(tokenDescriptor);
					var tokenString = tokenHandler.WriteToken(token);

					return Ok(new { Token = tokenString });
				}
				else
				{
					return Ok("Gagal cuy, coba lagi!");
				}
			}
			return Ok("Gagal Bro!");
		}

		[HttpGet("getCurrentUser")]
		public async Task<IActionResult> getCurrentUser()
		{

			string username = HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;

			return Ok(new { Username = username });
		}

		[HttpGet("listUsers")]
		public async Task<List<UserModel>> List()
		{
			List<WebApplication2User> users = new List<WebApplication2User>();
			users = await _userManager.Users.ToListAsync();
			List<UserModel> list = new List<UserModel>();
			foreach (WebApplication2User user in users)
			{
				list.Add(new UserModel { Username = user.UserName,Email=user.Email,Password=user.PasswordHash });
			}

			return list;
		}
	}
}
