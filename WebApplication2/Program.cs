using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Text;
using WebApplication2.Areas.Identity.Data;
using WebApplication2.Data;
namespace WebApplication2
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);
			var connectionString = builder.Configuration.GetConnectionString("WebApplication2ContextConnection") ?? throw new InvalidOperationException("Connection string 'WebApplication2ContextConnection' not found.");

			builder.Services.AddDbContext<WebApplication2Context>(options => options.UseSqlServer(connectionString));

			builder.Services.AddDefaultIdentity<WebApplication2User>(options => options.SignIn.RequireConfirmedAccount = true).AddEntityFrameworkStores<WebApplication2Context>();


			// Add services to the container.

			builder.Services.AddAuthentication(x =>
			{
				x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
		   .AddJwtBearer(x =>
		   {
			   var key = Encoding.ASCII.GetBytes("ASZ_2421_LGS_KEY_KMDNAJQLWROPXZY%@$%{})*+_}|><");
			   x.Events = new JwtBearerEvents
			   {
				   OnTokenValidated = context =>
				   {
					   // WebAPIUser
					   var userMachine = context.HttpContext.RequestServices.GetRequiredService<UserManager<WebApplication2User>>();
					   var user = userMachine.GetUserAsync(context.HttpContext.User);


					   if (user == null)
					   {
						   context.Fail("UnAuthorized");
					   }

					   return Task.CompletedTask;
				   }
			   };
			   x.RequireHttpsMetadata = false;
			   x.SaveToken = true;
			   x.TokenValidationParameters = new TokenValidationParameters
			   {
				   ValidateIssuerSigningKey = true,
				   IssuerSigningKey = new SymmetricSecurityKey(key),
				   ValidateIssuer = false,
				   ValidateAudience = false
			   };
		   });


			builder.Services.AddControllers();
			// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
			builder.Services.AddEndpointsApiExplorer();
			builder.Services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
				c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
				{
					Description = "Just type the decription that you like",
					In = ParameterLocation.Header,
					Name = "Authorization",
					Type = SecuritySchemeType.ApiKey
				});
				c.OperationFilter<SecurityRequirementsOperationFilter>();
			});


			var app = builder.Build();

			// Configure the HTTP request pipeline.
			if (app.Environment.IsDevelopment())
			{
				app.UseSwagger();
				app.UseSwaggerUI();
			}

			app.UseHttpsRedirection();
               app.UseAuthentication();;

			app.UseAuthorization();


			app.MapControllers();

			app.Run();
		}
	}
}